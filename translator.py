import os
import requests
import json
from window import Window


API_URLS = ['trans.zillyhuhn.com', 'translate.mentality.rip', 'translate.argosopentech.com', 'translate.api.skitzen.com']


class Translator():

    def __init__(self):
        """Create a translator instance with a window, api url,
        lists of available languages codes and names with corresponding index,
        combobox for original and translated languages,
        boxes for original and translated texts,
        translate and clear buttons."""
        self.window = Window()
        # set api_url
        for i in range(len(API_URLS)):
            self.api_url = API_URLS[i]
            try:
                # languages
                self.languages_codes = []
                self.language_names = []
                response = requests.get(f"https://{self.api_url}/languages").json()
                for language in response:
                    self.languages_codes.append(language['code'])
                    self.language_names.append(language['name'])
                break
            except Exception:
                i += 1                
        # Column 0
        # original language - row 0
        self.original_language = self.window.create_combobox(self.language_names, 'English', 0)
        # original text - row 1
        self.original_text = self.window.create_textbox('normal', 'white', 0)
        self.original_text.focus_set()
        self.original_text.bind("<KeyRelease>", lambda e: self.update_counter())
        self.original_text.bind("<Button-3><ButtonRelease-3>", self.window.show_full_menu)
        # counter of characts - row 2
        self.counter = self.window.create_label("0/5000")
        # Column 1
        # Interverting button - row 0
        img_arrow_url = f"{os.path.dirname(os.path.realpath(__file__))}/img/arrow.png"
        self.window.create_button("", self.intervert, 0, py=(10, 0), img_path=img_arrow_url)
        # Translate button - row 1
        self.window.create_button("Translate", self.translate, 1, px=5, font_size=(None, 14))
        # Clear button - row 2
        self.window.create_button("Clear", self.clear, 2, py=(0, 10), font_size=(None, 10))
        # column 2
        # translated language - row 0
        self.translated_language = self.window.create_combobox(self.language_names, 'French', 2)
        # translated text - row 1
        self.translated_text = self.window.create_textbox('disabled', '#d9d9d9', 2)
        self.translated_text.bind("<Button-3><ButtonRelease-3>", self.window.show_copy_select_menu)
        # api url - row 2
        self.window.create_option_menu(self.api_url, API_URLS, self.update_api_url)

    def update_counter(self):
        """Update the counter and limit number of charact in original text box."""
        n_charact = len(self.original_text.get(1.0, 'end-1c'))
        if n_charact > 5000:
            # limit number of charact
            s = self.original_text.get(1.0, "end-1c")
            self.original_text.delete(1.0, "end-1c")
            self.original_text.insert(1.0, s[:5000])
            n_charact = 5000
        self.counter.config(text=f"{str(n_charact)}/5000")  # update counter
    
    def intervert(self):
        """Intervert languages."""
        new_original_language = self.translated_language.get()
        new_translated_language = self.original_language.get()
        self.original_language.current(
            self.language_names.index(new_original_language))
        self.translated_language.current(
            self.language_names.index(new_translated_language))

    def translate(self):
        """Request LibreTranslate API to translate original text
        from original to tranlated language and insert the response
        in corresponding text box."""
        # delete previous translated message
        self.delete_tranlated_text()
        # make data
        original_language_code = self.languages_codes[
            self.language_names.index(self.original_language.get())]
        translated_language_code = self.languages_codes[
            self.language_names.index(self.translated_language.get())]
        data = {
            'q': self.original_text.get(1.0, 'end-1c'),
            'source': original_language_code,
            'target': translated_language_code,
            'format': "text"
        }
        try:
            # request api
            response = requests.post(
                f"https://{self.api_url}/translate",
                data=json.dumps(data),
                headers={"Content-Type": "application/json"},
            ).json()
            # insert translated message
            self.translated_text.configure(state='normal')
            self.translated_text.insert(1.0, response['translatedText'])
            self.translated_text.configure(state='disable')
        except KeyError:
            if self.original_text.get(1.0, 'end-1c') != "":
                self.window.create_error(response['error'])
        except Exception as e:
            self.window.create_error(e)

    def delete_tranlated_text(self):
        """Change state to normal before delete text and
        change state to disable."""
        self.translated_text.configure(state='normal')
        self.translated_text.delete(1.0, 'end')
        self.translated_text.configure(state='disable')

    def clear(self):
        """Delete all text boxes"""
        self.original_text.delete(1.0, 'end')
        self.delete_tranlated_text()
        self.counter.config(text="0/5000")  # update counter
    
    def update_api_url(self, new_url):
        """Update API url."""
        self.api_url = new_url

    def run(self):
        """Launch window loop"""
        self.window.mainloop()
