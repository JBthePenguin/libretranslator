from tkinter import Tk, Text, Button, Label, PhotoImage, Menu, StringVar, Frame
from tkinter.ttk import Combobox, Style, OptionMenu
from tkinter.messagebox import showerror


class CopySelectAllMenu(Menu):

    def __init__(self, window):
        """Create menu with copy and select all options."""
        super(CopySelectAllMenu, self).__init__(window, tearoff=0)
        self.add_command(label="Copy")
        self.add_separator()
        self.add_command(label="Select all")
    
    def configure(self, e_widget):
        """Configure menu with copy and select options."""
        self.entryconfigure("Copy",command=lambda: e_widget.event_generate("<<Copy>>"))
        self.entryconfigure("Select all",command=lambda: e_widget.tag_add("sel", "1.0","end-1c"))
    
    def call(self, event):
        """Call the menu."""
        self.tk.call("tk_popup", self, event.x_root, event.y_root)


class FullMenu(CopySelectAllMenu):

    def __init__(self, window):
        """Create menu with copy, cut, paste and select options."""
        super(FullMenu, self).__init__(window)
        self.insert_command(1, label="Cut")
        self.insert_command(2, label="Paste")
    
    def configure(self, e_widget):
        """Configure menu with copy, cut, paste and select options."""
        super(FullMenu, self).configure(e_widget)
        self.entryconfigure("Cut",command=lambda: e_widget.event_generate("<<Cut>>"))
        self.entryconfigure("Paste",command=lambda: e_widget.event_generate("<<Paste>>"))
        


class Window(Tk):

    def __init__(self):
        """Create gui window with title, background color, min size,
        grid configuration to resize TextBoxes when window is resized,
        Combobox style to set white color for readonly background."""
        super(Window, self).__init__()
        self.title('LibreTranslator')
        self['background'] = "gray"
        self.minsize(825, 302)
        # grid configuration
        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(2, weight=1)
        self.grid_rowconfigure(1, weight=1)
        # Combobox style
        style = Style()
        style.theme_use('alt')
        style.map('TCombobox', fieldbackground=[('readonly', 'white')])
        # Menus
        self.copy_select_menu = CopySelectAllMenu(self)
        self.full_menu = FullMenu(self)

    def create_combobox(self, box_value, default_value, ind_column):
        """Return created combobox with specified list of value, readonly state,
        specified default value, placed in grid row 0  and specified column,
        padding y 10,0 and removed highlighting for selected value."""
        combobox = Combobox(self, value=box_value, state="readonly")
        combobox.current(box_value.index(default_value))
        combobox.grid(row=0, column=ind_column, pady=(10, 0))
        combobox.bind("<<ComboboxSelected>>", lambda e: combobox.selection_clear())
        return combobox

    def create_textbox(self, box_state, bg_color, ind_column):
        """Return created textbox sized with specified state, word wrap,
        specified background color, placed in grid row 0 and specified column,
        padding 10 and sticky in all directions to resize it."""
        textbox = Text(
            self, height=10, width=40, state=box_state, wrap='word', bg=bg_color)
        textbox.grid(row=1, column=ind_column, pady=10, padx=10, sticky='nsew')
        return textbox
    
    def create_label(self, label_text):
        """Return created label with specified text, font size,
        placed in grid row 2 and column 0."""
        label = Label(self, text=label_text, font=(None, 7))
        label.grid(row=2, column=0)
        return label

    def create_button(
        self, button_text, command_func, ind_row, px=0, py=0, img_path=None, font_size=None):
        """Return created button with specified text, specified font size,
        specified command, placed in grid specified row and column 1,
        specified padding."""
        if img_path:
            image = PhotoImage(file=img_path)
            button = Button(self, command=command_func, image=image)
            button.image = image
        else:
            button = Button(self, text=button_text, font=font_size, command=command_func)
        button.grid(row=ind_row, column=1, padx=px, pady=py)

    def create_error(self, message):
        """Display error in message box."""
        showerror("LibreTranslator", message)
    
    def show_full_menu(self, event):
        """Open a cut, copy, paste and select menu."""
        self.full_menu.configure(event.widget)
        self.tk.call("tk_popup", self.full_menu, event.x_root, event.y_root)
    
    def show_copy_select_menu(self, event):
        """Open a copy and select menu."""
        self.copy_select_menu.configure(event.widget)
        self.tk.call("tk_popup", self.copy_select_menu, event.x_root, event.y_root)
    
    def create_option_menu(self, default_value, values, command_func):
        """Return created option menu with default value, specified list of value,
        placed in grid row 2  and column 2,
        padding y 15."""
        frame = Frame(self, bg='gray', pady=15)
        frame.grid(column=2, row=2)
        label = Label(frame,  text='API url : ', bg='gray')
        label.grid(column=0, row=0)
        option_menu = OptionMenu(
            frame,
            StringVar(self),
            default_value,
            *values,
            direction='above',
            command=command_func)
        option_menu.grid(column=1, row=0)
        return option_menu
