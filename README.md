# LibreTranslator

Translator using LibreTranslate API

## Requirements


### Requirements

You need linux package `python3-tk` and python library `requests`:
``` sh
sudo apt-get install python3-tk
pip3 install requests
```

## Usage

Clone or download this repository:
```sh
git clone https://gitlab.com/JBthePenguin/libretranslator.git
```
Run application:
```sh
python3 libretranslator
```

## Available API urls

This is a list of online resources that serve the LibreTranslate API.

URL |API Key Required|Contact|Cost
--- | --- | --- | ---
[libretranslate.com](https://libretranslate.com)|:heavy_check_mark:|[UAV4GEO](https://buy.stripe.com/28obLvdgGcIE5AQfYY)| [$9 / month](https://buy.stripe.com/28obLvdgGcIE5AQfYY), 80 requests / minute limit
[translate.mentality.rip](https://translate.mentality.rip)|-|-
[translate.argosopentech.com](https://translate.argosopentech.com/)|-|-
[translate.api.skitzen.com](https://translate.api.skitzen.com/)|-|-
[trans.zillyhuhn.com](https://trans.zillyhuhn.com/)|-|-


## License

[GNU General Public License 3](https://www.gnu.org/licenses/gpl-3.0.en.html)
